package com.svn.cst0.task.endpoint.web.controller;

import com.svn.cst0.task.core.calculation.InstrumentMetrics;
import com.svn.cst0.task.core.calculation.MetricType;
import com.svn.cst0.task.core.service.InstrumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
    @Autowired
    private InstrumentService instrumentService;
    @Autowired
    private InstrumentMetrics instrumentMetrics;

    @RequestMapping(value = "uploadInstruments", method = RequestMethod.GET)
    public String uploadInstrumentsByFilepath(@RequestParam(value = "filepath") String filepath){
        instrumentService.uploadInstruments(filepath);
        return "Completed ["+filepath+"]";

    }

    @RequestMapping(value = "metric", method = RequestMethod.GET)
    public String getMetric(@RequestParam(value = "metricType") String metricTypeCode,
                            @RequestParam(value = "instrument") String instrumentName){
        MetricType metricType = MetricType.valueOf(metricTypeCode);
        double value = instrumentMetrics.get(metricType, instrumentName);
        return "Computed metric["+metricType+"] instrument["+instrumentName+"] value["+value+"]";

    }

}
