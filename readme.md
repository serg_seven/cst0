To run the application, execute:

mvn clean install jetty:run

Then jetty web server will be started on port 8080, and H2 client web server on port 8082.

There is supposed use browser to interact with application.

NOTE: this looks like RESTful API, but it doesn't.

Supported actions:
1. Upload instruments file from location where web server was started.
Example:
  http://localhost:8080/uploadInstruments?filepath=d:\big_feed5.txt
  http://localhost:8080/uploadInstruments?filepath=/folder/big_feed5.txt (in case of unix-like systems. Hasn't tested, but I hope it works :) )

2. Calculate metric for uploaded instruments.
NOTE: all calculations can be done "on-the-fly", on the next browser's tab.

  URL format is
  http://localhost:8080/metric?metricType=<metric_type>&instrument=<instrument_name>
  where <metric_type> - one of supported metric type codes.
        <instrument_name> - instrument name for SUM_OF_THE_INSTRUMENT metric.
The list of supported metrics:
  MEAN - mean ofN specified instrument
  MEAN_FOR_NOVEMBER_2014 - mean of November 2014 for specified instrument
  MAX - max value of specified instrument
  MIN - min value of specified instrument
  SUM_OF_10_NEWEST - sum of the newest(by date) 10 elements of specified instrument name

Example:
  http://localhost:8080/metric?metricType=MEAN&instrument=INSTRUMENT1
  http://localhost:8080/metric?metricType=MEAN_FOR_NOVEMBER_2014&instrument=INSTRUMENT2
  http://localhost:8080/metric?metricType=MAX&instrument=INSTRUMENT3
  http://localhost:8080/metric?metricType=MIN&instrument=INSTRUMENT4
  http://localhost:8080/metric?metricType=SUM_OF_10_NEWEST&instrument=INSTRUMENT123


TODOs:
  - char as instrument_name type
  - use spring cache abstraction for instrument multiplier
  - db connection pool
  - set up jetty run-forked to be able increase java heap size 




