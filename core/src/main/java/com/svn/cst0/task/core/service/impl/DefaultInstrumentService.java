package com.svn.cst0.task.core.service.impl;

import com.svn.cst0.task.core.dao.InstrumentDao;
import com.svn.cst0.task.core.domain.Instrument;
import com.svn.cst0.task.core.parse.InstrumentSequenceParser;
import com.svn.cst0.task.core.service.InstrumentService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static com.google.common.base.Preconditions.checkArgument;

@Service
public class DefaultInstrumentService implements InstrumentService {
    @Autowired
    private InstrumentSequenceParser instrumentSequenceParser;
    @Autowired
    private InstrumentDao instrumentDao;

    @Override
    public void uploadInstruments(String filepath) {
        checkArgument(StringUtils.isNotBlank(filepath));

        Path path = Paths.get(filepath);
        checkArgument(Files.isReadable(path), "File is not readable by path: " + filepath);

        try(BufferedReader bufferedReader = Files.newBufferedReader(path)) {
            instrumentSequenceParser.parse(
                    instrument -> instrumentDao.save(instrument), bufferedReader);
        } catch (IOException e) {
            throw new RuntimeException("Error while uploading: "+e.getMessage(), e);
        }
    }
}
