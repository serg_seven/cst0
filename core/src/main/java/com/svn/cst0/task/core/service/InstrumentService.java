package com.svn.cst0.task.core.service;

import java.math.BigDecimal;

public interface InstrumentService {
    void uploadInstruments(String filepath);
}
