package com.svn.cst0.task.core.calculation.impl;

import com.svn.cst0.task.core.calculation.InstrumentMetrics;
import com.svn.cst0.task.core.calculation.MetricType;
import com.svn.cst0.task.core.dao.InstrumentDao;
import com.svn.cst0.task.core.domain.Instrument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;
import java.util.Optional;

@Component
public class DefaultInstrumentMetrics implements InstrumentMetrics {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private InstrumentDao instrumentDao;

    @Override
    public double get(MetricType metricType, String instrumentName) {
        switch (metricType) {
            case MEAN:
                return calculateMeanFor(instrumentName);
            case MEAN_FOR_NOVEMBER_2014:
                return calculateMeanFor(instrumentName,
                        Optional.of(LocalDate.of(2014, Month.NOVEMBER, 1)),
                        Optional.of(LocalDate.of(2014, Month.NOVEMBER, 30)));
            case MAX:
                return calculateMaxFor(instrumentName);
            case MIN:
                return calculateMinFor(instrumentName);
            case SUM_OF_10_NEWEST:
                return calculateSumOf10Newest(instrumentName);

            default:
                throw new RuntimeException(metricType + " isn't implemented yet.");
        }
    }

    private double calculateSumOf10Newest(String instrumentName) {
        List<Instrument> instruments = instrumentDao.findByNameNewest(instrumentName, 10);

        double sum = instruments.parallelStream().map(instrument -> {
            instrumentDao.findMultiplier(instrument.getName())
                    .ifPresent(multiplier -> instrument.setValue(instrument.getValue() * multiplier));

            return instrument;
        }).mapToDouble(Instrument::getValue).sum();

        return sum;
    }

    private double calculateMaxFor(String instrumentName) {
        List<Instrument> instruments = instrumentDao.findByName(instrumentName);

        double max = instruments.parallelStream().map(instrument -> {
            instrumentDao.findMultiplier(instrument.getName())
                    .ifPresent(multiplier -> instrument.setValue(instrument.getValue() * multiplier));

            return instrument;
        }).mapToDouble(Instrument::getValue).max().orElse(0d);

        return max;
    }

    private double calculateMinFor(String instrumentName) {
        List<Instrument> instruments = instrumentDao.findByName(instrumentName);

        double min = instruments.parallelStream().map(instrument -> {
            instrumentDao.findMultiplier(instrument.getName())
                    .ifPresent(multiplier -> instrument.setValue(instrument.getValue() * multiplier));

            return instrument;
        }).mapToDouble(Instrument::getValue).min().orElse(0d);

        return min;
    }

    private double calculateMeanFor(String instrumentName, Optional<LocalDate> from, Optional<LocalDate> to) {
        List<Instrument> instruments = from.isPresent() && to.isPresent() ?
                instrumentDao.findByNameAndDateFrame(instrumentName, from.get(), to.get()) :
                instrumentDao.findByName(instrumentName);

        double average = instruments.parallelStream().map(instrument -> {
            instrumentDao.findMultiplier(instrument.getName())
                    .ifPresent(multiplier -> instrument.setValue(instrument.getValue() * multiplier));

            return instrument;
        }).mapToDouble(Instrument::getValue).average().orElse(0d);

        return average;
    }

    private double calculateMeanFor(String instrumentName) {
        return calculateMeanFor(instrumentName, Optional.empty(), Optional.empty());
    }
}
