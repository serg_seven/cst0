package com.svn.cst0.task.core.parse.impl;

import com.svn.cst0.task.core.domain.Instrument;
import com.svn.cst0.task.core.parse.InstrumentItemParser;
import com.svn.cst0.task.core.parse.InstrumentSequenceParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.List;
import java.util.Scanner;
import java.util.function.Consumer;
import java.util.function.Function;

import static com.google.common.base.Preconditions.checkNotNull;

@Component
public class DefaultInstrumentSequenceParser implements InstrumentSequenceParser {
    @Autowired
    private InstrumentItemParser instrumentLineParser;

    @Override
    public void parse(List<Instrument> target, InputStream source) throws IOException {
        parse(target, new InputStreamReader(source));
    }

    @Override
    public void parse(List<Instrument> target, Reader source) throws IOException {
        parse(instrument -> target.add(instrument), source);
    }

    @Override
    public void parse(Consumer<Instrument> consumer, Reader source) throws IOException {
        checkNotNull(consumer);
        checkNotNull(source);

        Scanner fileScanner = new Scanner(source);
        while (fileScanner.hasNext()){
            String line = fileScanner.nextLine();

            Instrument instrument = instrumentLineParser.parse(line);
            consumer.accept(instrument);
        }
    }
}