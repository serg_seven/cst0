package com.svn.cst0.task.core.parse.impl;

import com.svn.cst0.task.core.domain.Instrument;
import com.svn.cst0.task.core.parse.InstrumentItemParser;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Scanner;

import static com.google.common.base.Preconditions.checkArgument;

@Component
public class DefaultInstrumentItemParser implements InstrumentItemParser {
    private static final DateTimeFormatter DATE_FIELD_FORMATTER = DateTimeFormatter
            .ofPattern("dd-MMM-yyyy")
            .withLocale(Locale.US);

    @Override
    public Instrument parse(String text) {
        checkArgument(StringUtils.isNotBlank(text));

        Instrument instrument = new Instrument();
        Scanner scanner = new Scanner(text)
                .useDelimiter(",")
                .useLocale(Locale.US);

        instrument.setName(scanner.next());
        instrument.setDate(LocalDate.parse(scanner.next(), DATE_FIELD_FORMATTER));
        instrument.setValue(scanner.nextDouble());

        return instrument;
    }
}
