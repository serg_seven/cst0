package com.svn.cst0.task.core.calculation;

import java.math.BigDecimal;

public interface InstrumentMetrics {
    double get(MetricType metricType, String instrumentName );
}
