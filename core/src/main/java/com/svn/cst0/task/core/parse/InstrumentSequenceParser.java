package com.svn.cst0.task.core.parse;

import com.svn.cst0.task.core.domain.Instrument;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.List;
import java.util.function.Consumer;

public interface InstrumentSequenceParser {
    void parse(List<Instrument> target, InputStream source) throws IOException;
    void parse(List<Instrument> target, Reader source) throws IOException;
    void parse(Consumer<Instrument> consumer, Reader source) throws IOException;

}
