package com.svn.cst0.task.core.dao.impl;

import com.svn.cst0.task.core.dao.InstrumentDao;
import com.svn.cst0.task.core.domain.Instrument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public class DefaultInstrumentDao implements InstrumentDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public Instrument save(Instrument object) {
        jdbcTemplate.update(
                "merge into instrument (name, effective_date, value) key(name, effective_date) values(?, ?, ?)",
                new Object[]{object.getName(), Date.valueOf(object.getDate()), object.getValue()});
        return object;
    }

    @Override
    public void saveAll(Iterable<Instrument> objects) {
        objects.forEach(object -> save(object));
    }

    public boolean isExist(Instrument object) {
        return jdbcTemplate.queryForRowSet(
                "select 1 from instrument where name = ? and effective_date = ?",
                new Object[]{object.getName(), Date.valueOf(object.getDate())}).next();
    }

    @Override
    public List<Instrument> findByName(String name) {
        return jdbcTemplate.query("select NAME, EFFECTIVE_DATE, VALUE from INSTRUMENT where NAME = ?",
                new Object[]{name}, (rs, rowNum) -> {
                    Instrument instrument = new Instrument();
                    instrument.setName(rs.getString("NAME"));
                    instrument.setDate(rs.getDate("EFFECTIVE_DATE").toLocalDate());
                    instrument.setValue(rs.getDouble("VALUE"));
                    return instrument;
                });
    }

    @Override
    public List<Instrument> findByNameAndDateFrame(String name, LocalDate from, LocalDate to) {
        return jdbcTemplate.query(
                "select NAME, EFFECTIVE_DATE, VALUE from INSTRUMENT where NAME = ? and EFFECTIVE_DATE between ? and ?",
                new Object[]{name, Date.valueOf(from), Date.valueOf(to)}, (rs, rowNum) -> {
                    Instrument instrument = new Instrument();
                    instrument.setName(rs.getString("NAME"));
                    instrument.setDate(rs.getDate("EFFECTIVE_DATE").toLocalDate());
                    instrument.setValue(rs.getDouble("VALUE"));
                    return instrument;
                });
    }

    // TODO get value from cache
    @Override
    public Optional<Double> findMultiplier(String name) {
        Double multiplier = jdbcTemplate.queryForObject("select top 1 MULTIPLIER from INSTRUMENT_MULTIPLIER where NAME = ?",
                new Object[]{name}, Double.class);
        return Optional.ofNullable(multiplier);
    }

    @Override
    public List<Instrument> findByNameNewest(String name, int amount) {
        return jdbcTemplate.query(
                "select top " + amount + " NAME, EFFECTIVE_DATE, VALUE from INSTRUMENT where NAME = ? order by EFFECTIVE_DATE DESC",
                new Object[]{name}, (rs, rowNum) -> {
                    Instrument instrument = new Instrument();
                    instrument.setName(rs.getString("NAME"));
                    instrument.setDate(rs.getDate("EFFECTIVE_DATE").toLocalDate());
                    instrument.setValue(rs.getDouble("VALUE"));
                    return instrument;
                });
    }
}
