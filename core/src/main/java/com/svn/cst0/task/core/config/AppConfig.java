package com.svn.cst0.task.core.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.svn.cst0.task.core")
public class AppConfig {
}
