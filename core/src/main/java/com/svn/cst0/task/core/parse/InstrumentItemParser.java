package com.svn.cst0.task.core.parse;

import com.svn.cst0.task.core.domain.Instrument;

public interface InstrumentItemParser {

    Instrument parse(String text);
}
