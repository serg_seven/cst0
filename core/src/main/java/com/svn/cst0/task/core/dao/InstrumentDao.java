package com.svn.cst0.task.core.dao;

import com.svn.cst0.task.core.domain.Instrument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface InstrumentDao {
    Instrument save(Instrument instrument);

    void saveAll(Iterable<Instrument> items);

    boolean isExist(Instrument instrument);

    List<Instrument> findByName(String name);

    List<Instrument> findByNameAndDateFrame(String name, LocalDate from, LocalDate to);

    Optional<Double> findMultiplier(String name);

    List<Instrument> findByNameNewest(String instrumentName, int amount);
}
