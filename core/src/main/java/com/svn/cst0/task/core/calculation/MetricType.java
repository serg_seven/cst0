package com.svn.cst0.task.core.calculation;

public enum MetricType {
    /** mean of specified instrument name */
    MEAN,
    /** mean of specified instrument name for November 2014*/
    MEAN_FOR_NOVEMBER_2014,
    /** maximum value for specified instrument name */
    MAX,
    /** maximum value for specified instrument name */
    MIN,
    /** sum of the newest(by date) 10 elements for specified instrument name */
    SUM_OF_10_NEWEST
}
