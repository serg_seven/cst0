package com.svn.cst0.task.core.calculation.impl;

import com.svn.cst0.task.core.calculation.MetricType;
import com.svn.cst0.task.core.config.AppConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AppConfig.class})
public class DefaultInstrumentMetricsTest {
    @Autowired
    private DefaultInstrumentMetrics defaultInstrumentMetrics;

    @Test
    public void shouldGetAverageValueForInstrument1(){
        double average = defaultInstrumentMetrics.get(MetricType.MEAN, "INSTRUMENT1");
        assertEquals(19293.82698492d, average, 0d);
    }

}