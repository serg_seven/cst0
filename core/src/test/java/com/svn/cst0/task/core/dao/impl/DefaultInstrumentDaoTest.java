package com.svn.cst0.task.core.dao.impl;

import com.svn.cst0.task.core.config.AppConfig;
import com.svn.cst0.task.core.domain.Instrument;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDate;
import java.time.Month;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AppConfig.class})
public class DefaultInstrumentDaoTest {
    @Autowired
    private DefaultInstrumentDao defaultInstrumentDao;

    @Test
    public void testIsExistOnExistingObject() {
        Instrument instrument = new Instrument();
        instrument.setName("INSTRUMENT1");
        instrument.setDate(LocalDate.of(1999, Month.OCTOBER, 22));
        assertTrue(defaultInstrumentDao.isExist(instrument));
    }

    @Test
    public void testIsExistOnNonExistingObject() {
        Instrument instrument = new Instrument();
        instrument.setName("NOT_EXISTS");
        instrument.setDate(LocalDate.of(1995, Month.JANUARY, 15));
        assertFalse(defaultInstrumentDao.isExist(instrument));
    }

    @Test
    public void testIsExistOnExistingObject2() {
        Instrument instrument = new Instrument();
        instrument.setName("NOT_EXISTS");
        instrument.setDate(LocalDate.of(1999, Month.OCTOBER, 22));
        assertFalse(defaultInstrumentDao.isExist(instrument));
    }

    @Test
    public void testCreateNew() throws Exception {
        Instrument instrument = new Instrument();
        instrument.setName("INSTRUMENT1");
        instrument.setDate(LocalDate.of(1979, Month.OCTOBER, 22));
        instrument.setValue(123.456d);

        assertFalse(defaultInstrumentDao.isExist(instrument));

        defaultInstrumentDao.save(instrument);

        assertTrue(defaultInstrumentDao.isExist(instrument));
    }


    @Test
    public void testUpdateExisting() throws Exception {
        Instrument instrument = new Instrument();
        instrument.setName("INSTRUMENT1");
        instrument.setDate(LocalDate.of(1999, Month.OCTOBER, 22));
        instrument.setValue(123.456d);

        assertTrue(defaultInstrumentDao.isExist(instrument));

        defaultInstrumentDao.save(instrument);

        assertTrue(defaultInstrumentDao.isExist(instrument));
    }
}