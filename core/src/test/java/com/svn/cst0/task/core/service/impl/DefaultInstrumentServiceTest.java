package com.svn.cst0.task.core.service.impl;

import com.svn.cst0.task.core.config.AppConfig;
import com.svn.cst0.task.core.service.InstrumentService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AppConfig.class})
public class DefaultInstrumentServiceTest {
    @Autowired
    private DefaultInstrumentService defaultInstrumentService;

    @Test
    public void shouldUploadExampleInputByFilepath() throws URISyntaxException {
        Path filepath = Paths.get(getClass().getResource("/input/example_input.txt").toURI());
        defaultInstrumentService.uploadInstruments(filepath.toString());
    }

}