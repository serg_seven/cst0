package com.svn.cst0.task.core.parse.impl;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Random;

public class InstrumentFeeder {
    private static final int LINES = 3 * 23000000; // 3gb
    private static final int INSTRUMENTS = 10000;
    private static final DateTimeFormatter DATE_FIELD_FORMATTER = DateTimeFormatter
            .ofPattern("dd-MMM-yyyy")
            .withLocale(Locale.US);



    public static void main2(String[] args) throws IOException {
        Path file = Files.createFile(Paths.get("d://big_feed5.txt"));
        try(BufferedWriter bufferedWriter = Files.newBufferedWriter(file)) {

            Random random = new Random();

            for (int i = 0; i < LINES; i++) {
                int instrumentNum = random.nextInt(INSTRUMENTS);
                LocalDate date = LocalDate.of(random.nextInt(25) + 1990, random.nextInt(12) + 1, random.nextInt(28) + 1);
                double value = random.nextDouble() * 1000;

                StringBuilder sb = new StringBuilder()
                        .append("INSTRUMENT").append(instrumentNum).append(",")
                        .append(date.format(DATE_FIELD_FORMATTER)).append(",")
                        .append(value);

                bufferedWriter.append(sb);
                bufferedWriter.newLine();
            }

            bufferedWriter.flush();
        }
    }

    public static void main(String[] args) throws IOException {
        Random random = new Random();
        for(int i=1; i<5000; i++){
            int instrumentNumber = random.nextInt(INSTRUMENTS);
            double multiplier = random.nextDouble();
            System.out.println("INSERT INTO INSTRUMENT_MULTIPLIER (NAME, MULTIPLIER) " +
                    "VALUES('INSTRUMENT"+instrumentNumber+"', "+multiplier+");");
        }
    }
}
