package com.svn.cst0.task.core.parse.impl;

import com.svn.cst0.task.core.config.AppConfig;
import com.svn.cst0.task.core.domain.Instrument;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.InputStream;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AppConfig.class})
public class DefaultInstrumentSequenceParserTest {
    @Autowired
    private DefaultInstrumentSequenceParser defaultInstrumentFileParser;

    @Test
    public void testFromFilepath() throws Exception {
        List<Instrument> instruments = new ArrayList<>();

        try(InputStream source = getClass().getResourceAsStream("/input/example_input.txt")) {
            defaultInstrumentFileParser.parse(instruments, source);
        }

        assertFalse(instruments.isEmpty());

        Instrument firstRow = instruments.get(0);

        assertEquals("INSTRUMENT1", firstRow.getName());
        assertEquals(LocalDate.of(1996, Month.JANUARY, 1), firstRow.getDate());
        assertEquals(2.4655d, firstRow.getValue(), 0d);


        assertEquals(instruments.size(), 14826);
    }
}