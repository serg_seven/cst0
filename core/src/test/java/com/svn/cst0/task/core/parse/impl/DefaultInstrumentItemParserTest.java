package com.svn.cst0.task.core.parse.impl;

import com.svn.cst0.task.core.domain.Instrument;
import org.junit.Test;

import static org.junit.Assert.*;

public class DefaultInstrumentItemParserTest {
    public static final String TEST_LINE_1 = "INSTRUMENT1,11-Jan-1996,2.4845";

    private DefaultInstrumentItemParser defaultInstrumentLineParser = new DefaultInstrumentItemParser();

    @Test
    public void shouldParseRegularInstrumentLine() throws Exception {
        Instrument instrument = defaultInstrumentLineParser.parse(TEST_LINE_1);
        assertNotNull(instrument.getName());
        assertNotNull(instrument.getDate());
        assertNotNull(instrument.getValue());
    }
}